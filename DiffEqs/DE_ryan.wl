(* ::Package:: *)

(* ::Section::Closed:: *)
(*Obtain the master integral basis*)


<<LiteRed`;
SetDirectory[NotebookDirectory[]];
SetDim[d];
Declare[{k1,k2,p1,p2,p3},Vector];


sp[p1,p1]=0;
sp[p2,p2]=0;
sp[p3,p3]=0;
sp[p1,p2]=s/2;
sp[p2,p3]=t/2;
sp[p1,p3]=-(s+t)/2;


props={k1,k1-p1,k1-p1-p2,k2-p1-p2,k2-p1-p2-p3,k2,k1-k2};
loops={k1,k2};


NewBasis[boxtwoloops,props,loops,Directory->"2L_box_basis", Append->True];


GenerateIBP[boxtwoloops]


AnalyzeSectors[boxtwoloops, {___,0,0}]


FindSymmetries[boxtwoloops,EMs->True]


ZeroSectors[boxtwoloops];
IBP[boxtwoloops];


SolvejSector/@UniqueSectors[boxtwoloops]


MIs[boxtwoloops]


(* ::Section:: *)
(*Obtain the differential equations*)


<<LiteRed`;
SetDirectory[NotebookDirectory[]];
SetDim[d];
Declare[{k1,k2,p1,p2,p3},Vector]
<<"2L_box_basis/boxtwoloops";
ExecuteDefinitions[boxtwoloops];


ds[pi_,pj_]:=Collect[1/2*IBPReduce[Dinv[MIs[boxtwoloops],sp[pi,pj]]],_j,Factor];


ds12=ds[p1,p2];
ds23=ds[p2,p3];
ds13=ds[p1,p3];


MIs[boxtwoloops]


Collect[2sp[p1,p2]ds12+2sp[p2,p3]ds23+2sp[p1,p3]ds13,_j,Factor]


(* ::Section:: *)
(*Change variables*)


ds=Collect[ds12-ds13,_j,Factor];
dt=Collect[ds23-ds13,_j,Factor];


Collect[s ds + t dt,_j,Factor]


d\[Sigma]=Collect[-ds-x dt/.{s->-\[Sigma],t->-x \[Sigma]},_j,Factor];
dx=Collect[-\[Sigma] dt /.{s->-\[Sigma],t->-x \[Sigma]},_j,Factor];


Ax=Table[Coefficient[dx[[i]],MIs[boxtwoloops][[j]],1]/.\[Sigma]->1,{i,8},{j,8}];
Axe=Ax/.d->4-2\[Epsilon]//Factor;
Axe//MatrixForm


(* ::Section:: *)
(*Gauge transformation*)


GaugeTransform[A_,M_,x_]:=Inverse[M].(A.M-D[M,x])


MM={{1/\[Epsilon]/(1-2\[Epsilon]),0,0},{0,1/\[Epsilon]/(1-2\[Epsilon]),0},{0,0,1/\[Epsilon]^2/x}};
MM//MatrixForm


Ax2=Simplify[GaugeTransform[Ax/.d->4-2\[Epsilon],MM,x]];


AxMatrix[i_]:={{-1/x[i],0,0},{0,0,0},{2/x[i]-2/(1+x[i]),-2/(1+x[i]),1/(1+x[i])-1/x[i]}}


\[Epsilon] AxMatrix[1]-Ax2/.{x[1]->x}//Simplify//MatrixForm


<<Libra`


NewDSystem[Axe2,x->Axe];


t=VisTransformation[Axe2,x,\[Epsilon]];


t={{1,0,0,0,0,0,0,0},{0,x,0,0,0,0,0,0},{0,0,1,0,0,0,0,0},{0,0,0,1,0,0,0,0},{0,0,0,0,1,0,0,0},{0,0,0,0,0,1/(1+x),0,0},{0,0,0,0,0,0,1/x,0},{0,0,0,0,0,0,0,1/(1+x)}};


Transform[Axe2,t];


Axe2[x]//MatrixForm


FuchsianQ[Axe2,x]


Block[{\[Mu]=1,C},C[1]=1;C[_]=0;
t=FactorOut[Axe2[x],x,eps,\[Mu]]]
Factor[Det[t]]=!=0



