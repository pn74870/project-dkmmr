(* ::Package:: *)

(* ::Section::Closed:: *)
(*Init*)


Format[spAA[i_,j_]]:=RowBox[{"\[LeftAngleBracket]",GridBox[{{i,j}}],"\[RightAngleBracket]"}]//DisplayForm;
Format[spBB[i_,j_]]:=RowBox[{"[",GridBox[{{i,j}}],"]"}]//DisplayForm;
spAA[i_,i_]:=0;
spBB[i_,i_]:=0;
s[i_,j_]:=spAA[i,j]*spBB[j,i];
s[i_,j_,k_]:=s[i,j]+s[i,k]+s[j,k];
Format[b[i_]]:=Subscript[\[Beta], i];


denominator=spBB[3,4]*spBB[4,5]*(spAA[2,1]*spBB[1,5]+spAA[2,6]*spBB[6,5])\
			*(spAA[6,1]*spBB[1,3]+spAA[6,2]*spBB[2,3])*s[3,4,5];


Get[NotebookDirectory[]<>"BlackBox.wl"];


(* ::Section::Closed:: *)
(*Twistors*)


n=6;
mkZ[p_]:=RandomInteger[{0,p-1},{4,n}];


circ[i_]:=Mod[i,n,1];
Zi[Z_,i_]:=Z[[;;,circ[i]]];
ZiA[Z_,i_,A_]:=Zi[Z,i][[A]];


getspA[Z_,i_]:=Z[[;;2,i]];


dospAA[Z_,p_]:=Flatten[
	Prepend[
		Table[spAA[i,j]->Mod[Module[
				{a=getspA[Z,i],b=getspA[Z,j]},
				spinorProduct[a,b]
			],p],
			{i,5},{j,i+1,6}
		],
		spAA[i_,j_]:>-spAA[j,i]/;i>j
	]
];


Wi[Z_,i_,p_,rulesA_]:=
	Mod[
		Mod[LeviCivitaTensor[4].Zi[Z,i-1].Zi[Z,i].Zi[Z,i+1],p]
			*ModularInverse[(spAA[circ[i-1],i]*spAA[i,circ[i+1]])//.rulesA,p],
		p
	];


mkW[Z_,p_,rulesA_]:=Wi[Z,#,p,rulesA]&/@Range[n];


getspB[W_,i_]:=W[[i,3;;]];


dospBB[W_,p_]:=Flatten[
	Prepend[
		Table[
			spBB[i,j]->Mod[Module[
				{a=getspB[W,i],b=getspB[W,j]},
				spinorProduct[a,b]
				],p],
			{i,5},{j,i+1,6}
		],
		spBB[i_,j_]:>-spBB[j,i]/;i>j
	]
];


getsps[p_]:=Module[
	{Z=mkZ[p],rulesA,W,point,rules},
	rulesA=dospAA[Z,p];
	W=mkW[Z,p,rulesA];
	point={getspA[Z,#]&/@Range[6],getspB[W,#]&/@Range[6]};
	rules=Join[{Power[a_,b_]:>PowerMod[a,b,p]},rulesA,dospBB[W,p]];
	Return[{rules,point}];
];


dosps[list_,p_]:=Module[
	{rules,pt},
	{rules,pt}=getsps[p];
	Mod[#//.rules,p]&/@list
];


checkMomCons[p_]:=(Total[KroneckerProduct@@@Thread[getsps[p][[2]]]]//Mod[#, p]&)=={{0,0},{0,0}};


p=NextPrime[2^31,-1];
checkMomCons[p]


(* ::Section:: *)
(*Ansatze*)


spw[spP_]:=If[spP[[0]]===spAA,+1,-1]*If[spP[[1]]==#||spP[[2]]==#,1,0]&/@Range[6];
findRowFirsts[entry_,matrix_]:=Module[
	{pos={}, i=1},
	While[
		Position[matrix[[i]],entry]!={},
		AppendTo[pos,Position[matrix[[i]],entry][[1,1]]];
		i++;
	];
	pos
];
coeffsFp[p_]:=Module[
	{vs,vl,cond1,cond2,sol,ln,g,G,Gr,pos,rl,gr,M,ns},
	vs=Table[{spAA[i,j],spBB[i,j]},{i,5},{j,i+1,6}]//Flatten;
	vl=2*Binomial[6,2];
	cond1=Sum[b[j],{j,vl}]==10;
	cond2=Sum[b[j]*spw[vs[[j]]],{j,vl}]=={-2,3,-4,0,-4,3};
	sol=Solve[{cond1,cond2},b/@Range[vl],NonNegativeIntegers];
	ln=Length[sol];

	g=Product[Power[vs[[j]],b[j]],{j,30}]/.sol[[#]]&/@Range[ln];
	G=dosps[g,p]&/@Range[ln];

	Gr=RowReduce[G,Modulus->p];
	pos=findRowFirsts[1,Gr];
	rl=Length[pos];
	gr=g[[#]]&/@pos;

	M=Module[
			{rules,pt},
			{rules,pt}=getsps[p];
			Prepend[Mod[gr//.rules,p],Mod[blackBox[pt,p]*(denominator//.rules),p]]
		]&/@Range[rl];
	ns=NullSpace[M,Modulus->p][[1]];
	ns=Mod[-ModularInverse[2,p]*#,p]&/@ns;
	{ns[[2;;]],gr}
];
rationalReconstruct[a_,n_]:=
	With[{v=LatticeReduce[{{a,1},{n,0}}][[1]]},v[[1]]/v[[2]]];


{cs0,gr0}=coeffsFp[p];
cs0=rationalReconstruct[#,p]&/@cs0;
numerator=Total[Times@@#&/@Transpose@{cs0,gr0}]//Simplify


schoutenRulePow={Power[spAA[i_,j_],n_]*spAA[k_,l_]:>Power[spAA[i,j],n-1]*(spAA[i,k]*spAA[j,l]+spAA[i,l]*spAA[k,j])};
schoutenRule={
	spAA[i_,j_]*spAA[k_,l_]:>spAA[i,k]*spAA[j,l]+spAA[i,l]*spAA[k,j],
	spBB[i_,j_]*spBB[k_,l_]:>spBB[i,k]*spBB[j,l]+spBB[i,l]*spBB[k,j]
	};
antisymmetry={(f:(spAA|spBB))[i_,j_]:>Signature[{i,j}]*f@@Sort[{i,j}]};


{rs,pt}=getsps[p];
Mod[numerator/denominator//.rs,p]==blackBox[pt,p]


(* ::Subsection::Closed:: *)
(*CRT*)


p1=NextPrime[2^15,-1]
cs1=coeffsFp[p1][[1]]


p2=NextPrime[p1,-1]
cs2=coeffsFp[p2][[1]]


p3=NextPrime[p2,-1]
cs3=coeffsFp[p3][[1]]


poss=Position[cs1,16374]//Flatten


ChineseRemainder[{cs1[[#]],cs2[[#]],cs3[[#]]},{p1,p2,p3}]&/@poss
